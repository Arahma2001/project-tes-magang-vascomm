<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;


class UserController extends Controller
{

    public function showDataUser()
    {
        return view('data-user');
    }

    public function getDataUser(Request $request)
    {
        if ($request->ajax()) {
            $data = User::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('photo', function($user){
                    $photo = view('view-photo', ['photo' => $user->photo]);
                    return $photo;
                })
                ->rawColumns(['photo'])
                ->make(true);
        }
    }

}

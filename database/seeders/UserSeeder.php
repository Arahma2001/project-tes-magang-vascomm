<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Bima',
                'email' => 'bima@gmail.com',
                'password' => Hash::make('bima2001'),
                'role' => 'admin',
                'photo' => 'bima.jpg',
            ], [
                'name' => 'Yoshi',
                'email' => 'yoshi@gmail.com',
                'password' => Hash::make('yoshi2001'),
                'role' => 'admin',
                'photo' => 'yoshi.png',
            ], [
                'name' => 'Farrel',
                'email' => 'farrel@gmail.com',
                'password' => Hash::make('farrel2001'),
                'role' => 'admin',
                'photo' => 'farrel.jpg',
            ],
        ];
        foreach($users as $user) {
            User::create($user);
        }

        User::factory()
            ->count(25)
            ->create();
    }
}

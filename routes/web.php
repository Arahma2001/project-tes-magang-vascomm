<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\LoginContoller;
use App\Http\Controllers\UserController;
// use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
// Route::get('/login', [LoginContoller::class, 'index'])->name('login');
// Route::get('/register', [UserController::class, 'showRegister'])->name('register');
// Route::post('/register', [UserController::class, 'createUser'])->name('register.post');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/data-user', [UserController::class, 'showDataUser'])->name('data-user')->middleware('auth');
Route::get('/data-user/list', [UserController::class, 'getDataUser'])->name('data-user.list')->middleware('auth');
